import discord
import json
from mediator import Events

import logging
logger = logging.getLogger(__name__)


class Bot(discord.Client):

    def __init__(self, config_filepath):
        super().__init__()

        with open(config_filepath) as f:
            self.config = json.load(f)


    def set_mediator(self, mediator):
        self.mediator = mediator


    async def on_ready(self):
        logger.info(f"{self.user} has logged in")


    async def on_voice_state_update(self, member, before, after):
        await self.mediator.update(Events.DISCORD_VOICE_STATE_UPDATE_RECEIVED, (member, before, after))


    async def on_message(self, message):
        if message.author == self.user:
            return

        await self.mediator.update(Events.DISCORD_MESSAGE_RECEIVED, (message, ))
    

    async def send_message(self, message):
        await self.get_channel(self.config["channel"]).send(message)


    async def start(self):
        await super().start(self.config["token"])
