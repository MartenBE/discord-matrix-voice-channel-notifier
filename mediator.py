import asyncio
from enum import Enum, auto

import logging
logger = logging.getLogger(__name__)


class Events(Enum):
    DISCORD_MESSAGE_RECEIVED = auto()
    DISCORD_VOICE_STATE_UPDATE_RECEIVED = auto()
    MATRIX_MESSAGE_RECEIVED = auto()


class Mediator():

    def __init__(self, matrixbot, discordbot, loop=None):
        self.matrixbot = matrixbot
        self.matrixbot.set_mediator(self)

        self.discordbot = discordbot
        self.discordbot.set_mediator(self)

        if loop == None:
            self.loop = asyncio.get_event_loop()
        else:
            self.loop = loop


    async def update(self, received_event, received_data):
        if received_event is Events.DISCORD_MESSAGE_RECEIVED:
            message = received_data[0]

            output = f"**{message.author.display_name}** in text channel **{message.channel}** on discord server **{message.guild}**: _{message.content}_"
            logger.info(f"DISCORD -> MATRIX | {output}")
            await self.matrixbot.send_message(output)

        elif received_event is Events.DISCORD_VOICE_STATE_UPDATE_RECEIVED:
            member = received_data[0]
            before = received_data[1]
            after = received_data[2]

            output = ""
            if not before.channel and after.channel:
                output = f"**{member.display_name}** joined voice channel **{after.channel.name}** on discord server **{after.channel.guild}**"

            elif before.channel and not after.channel:
                output = f"**{member.display_name}** left voice channel **{before.channel.name}** on discord server **{before.channel.guild}**"

            elif before.channel and after.channel and before.channel != after.channel:
                output = f"**{member.display_name}** changed voice channel from **{before.channel.name}** to **{after.channel.name}** on discord server **{before.channel.guild}**"

            if output:
                logger.info(f"DISCORD -> MATRIX | {output}")
                await self.matrixbot.send_message(output)

        elif received_event is Events.MATRIX_MESSAGE_RECEIVED:
            room = received_data[0]
            event = received_data[1]

            output = f"**{room.user_name(event.sender)}** in matrix room **{room.machine_name}**: _{event.body}_"
            logger.info(f"MATRIX -> DISCORD | {output}")
            await self.discordbot.send_message(output)

        else:
            raise ValueError()


    def run(self):
        self.loop.run_until_complete(self.matrixbot.login())
        self.loop.create_task(self.matrixbot.start())
        self.loop.create_task(self.discordbot.start())
        self.loop.run_forever()

