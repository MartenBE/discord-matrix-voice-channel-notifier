#!/usr/bin/env python3

import logging
import sys
from matrixbot.bot import Bot as MatrixBot
from discordbot.bot import Bot as DiscordBot
from mediator import Mediator

# Set up logging
logger = logging.getLogger(__name__)
format = "%(asctime)s | %(name)s [%(levelname)s] %(message)s"
logging.basicConfig(stream=sys.stdout, level=logging.INFO, format=format)

# Running the bot
mbot = MatrixBot("matrix.config.yaml")
dbot = DiscordBot("discord.config.json")
mediator = Mediator(mbot, dbot)
mediator.run() 