import os
import collections
from time import sleep
from nio import (
    AsyncClient,
    AsyncClientConfig,
    RoomMessageText,
    InviteMemberEvent,
    LoginError,
    LocalProtocolError,
)
from aiohttp import (
    ServerDisconnectedError,
    ClientConnectionError
)
from matrixbot.config import Config
from mediator import Events
from markdown import markdown

import logging
logger = logging.getLogger(__name__)


class Bot():

    def __init__(self, config_filepath):
        # Read config file
        self.config = Config(config_filepath)

        # Configuration options for the AsyncClient
        client_config = AsyncClientConfig(
            max_limit_exceeded=0,
            max_timeouts=0,
            store_sync_tokens=True,
            encryption_enabled=self.config.enable_encryption,
        )

        # Initialize the matrix client
        self.client = AsyncClient(
            self.config.homeserver_url,
            self.config.user_id,
            device_id=self.config.device_id,
            store_path=self.config.store_filepath,
            config=client_config,
        )

        # Set up event callbacks
        self.client.add_event_callback(self.on_message, (RoomMessageText,))
        self.client.add_event_callback(self.on_invite, (InviteMemberEvent,))


    async def login(self):
        try:
            # Try to login with the configured username/password
            try:
                login_response = await self.client.login(
                    password=self.config.user_password,
                    device_name=self.config.device_name,
                )

                # Check if login failed
                if type(login_response) == LoginError:
                    logger.error(f"Failed to login: %s", login_response.message)
                    return False
            except LocalProtocolError as e:
                # There's an edge case here where the user enables encryption but hasn't installed
                # the correct C dependencies. In that case, a LocalProtocolError is raised on login.
                # Warn the user if these conditions are met.
                if config.enable_encryption:
                    logger.fatal(
                        "Failed to login and encryption is enabled. Have you installed the correct dependencies? "
                        "https://github.com/poljar/matrix-nio#installation"
                    )
                    return False
                else:
                    # We don't know why this was raised. Throw it at the user
                    logger.fatal("Error logging in: %s", e)
                    return False

            # Login succeeded!

            # Sync encryption keys with the server
            # Required for participating in encrypted rooms
            if self.client.should_upload_keys:
                await self.client.keys_upload()

            # Trust your own devices
            self._trust_devices(self.config.user_id)

            logger.info(f"Logged in as {self.config.user_id}")

        except (ClientConnectionError, ServerDisconnectedError):
            logger.warning("Unable to connect to homeserver, retrying in 15s ...")

            # Sleep so we don't bombard the server with login requests
            sleep(15)


    def set_mediator(self, mediator):
        self.mediator = mediator


    async def on_message(self, room, event):
        # Ignore messages from ourselves
        if event.sender == self.client.user:
            return

        await self.mediator.update(Events.MATRIX_MESSAGE_RECEIVED, (room, event))


    async def on_invite(self, room, event):
        logger.debug(f"Got invite to {room.room_id} from {event.sender}.")

        # Attempt to join 3 times before giving up
        for attempt in range(3):
            result = await self.client.join(room.room_id)
            if type(result) == JoinError:
                logger.error(
                    f"Error joining room {room.room_id} (attempt %d): %s",
                    attempt, result.message,
                )
            else:
                logger.info(f"Joined {room.room_id}")
                break


    async def send_message(self, message):
        content = {
            "msgtype": "m.notice",
            "format": "org.matrix.custom.html",
            "body": message,
            "formatted_body": markdown(message)
        }

        try:
            await self.client.room_send(
                self.config.room_id,
                "m.room.message",
                content,
                ignore_unverified_devices=True,
            )
        except SendRetryError:
            logger.exception(f"Unable to send message response to {self.config.room_id}")


    def _trust_devices(self, user_id: str):
        for device_id, olm_device in self.client.device_store[user_id].items():
            if user_id != self.client.user_id or device_id != self.client.device_id:
                self.client.verify_device(olm_device)

                logger.info(f"Trusting {device_id} from user {user_id}")


    async def start(self):
        await self.client.sync_forever(timeout=30000, full_state=True, loop_sleep_time=1000)
