# Discord Matrix bridge bot

This is a simple bot facilitating 2 functions between Matrix and Discord supporting e2e:

* Send notifications to a Matrix room whenever someone joins, leaves or changes an audio channel on the Discord guild server
* Transfer messages of members between a Discord server and a Matrix room

This bot uses the Matrix python libraries [matrix-nio](https://github.com/poljar/matrix-nio) together with best practises for `matrix-nio` found in [nio-template](https://github.com/anoadragon453/nio-template).

## F.A.Q.

### Should I use this in production?

No, this bot is probably riddled with bugs (especially regarding the e2e) and is missing a lot of functionality (e.g. sending images etc.). It is more a proof of concept.